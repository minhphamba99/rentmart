import axios from "axios";

const API_URL = process.env.BACKEND_URL+ "categories/";

class CategoryService {
  list() {
    return axios.get(API_URL,).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }

}
export default new CategoryService();