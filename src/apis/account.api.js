import axios from "axios";
import axiosApiInstance from "../lib/axiosApiInstance";
import Cookie from "js-cookie";
const API_URL = process.env.BACKEND_URL + "auths/";

class AccountService {
  verify(data) {
    return axiosApiInstance.put(API_URL + "verify", data).then((response) => {
      return response.data.payload;
    });
  }

  login(idata) {
    return axios.post(API_URL + "signin", idata).then((response) => {
      const data = response.data.payload;
      if (data) {
        Cookie.set("accessToken", data.accessToken, { expires: 1 });
        Cookie.set("refreshToken", data.refreshToken, { expires: 1 });
      }
      return data;
    });
  }

  loginSocial(input) {
    return axios.post(API_URL + "social", input).then(async (response) => {
      if (response.data) {
        localStorage.setItem(
          "accessToken",
          JSON.stringify(response.data.accessToken)
        );
        localStorage.setItem(
          "refreshToken",
          JSON.stringify(response.data.refreshToken)
        );
      }
      return response.data;
    });
  }

  logout() {
    // return axiosApiInstance.post(API_URL + "logout").then((response) => {
    // localStorage.removeItem("accessToken");
    // localStorage.removeItem("refreshToken");
    Cookie.remove("accessToken");
    Cookie.remove("refreshToken");
    // });
  }

  async updatePassword(data) {
    return await axiosApiInstance.put(API_URL + "update-password", data);
  }
}
export default new AccountService();
