import axios from "axios";

const API_URL = process.env.BACKEND_URL+ "locations/";

class LocationService {
  getCities() {
    return axios.get(API_URL + "cities").then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }

  getDistricts(cityCode){
    return axios.get(API_URL + `cities/${cityCode}/districts`).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
  
  getWards(districtCode){
    return axios.get(API_URL + `districts/${districtCode}/wards`).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
}
export default new LocationService();
