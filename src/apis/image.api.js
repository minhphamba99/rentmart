import axios from "axios";
import axiosApiInstance from "../lib/axiosApiInstance";
import Cookie from "js-cookie";
const API_URL = process.env.BACKEND_URL + "images/";

class ImageService {
  postImage(images) {
    const formData = new FormData();
    images.forEach(file => {
      formData.append('images', file);
    });

    return axiosApiInstance.post(API_URL, formData).then((response) => {
      return response.data.payload;
    });
  }
}
export default new ImageService();
