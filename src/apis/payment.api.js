import axiosApiInstance from "../lib/axiosApiInstance";

const API_URL = process.env.BACKEND_URL+ "checkout/";
class PaymentService {
  checkout(returnUrl) {
    return axiosApiInstance
      .post(API_URL + "payment/", { returnUrl: returnUrl })
      .then((response) => {
        const idata = response.data.payload;
        return idata;
      });
  }
  getPaymentInfo() {
    return axiosApiInstance.get(API_URL + "payment/").then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
  getPaymentMethod() {
    return axiosApiInstance
      .get(API_URL + "payment-methods")
      .then((response) => {
        const idata = response.data.payload;
        return idata;
      });
  }
  putPaymentMethod(type) {
    return axiosApiInstance
      .put(API_URL + "payment-methods", { type: type })
      .then((response) => {
        const idata = response.data.payload;
        return idata;
      });
  }
}
export default new PaymentService();
