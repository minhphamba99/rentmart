import axios from "axios";
import axiosApiInstance from "../lib/axiosApiInstance";

const API_URL = process.env.BACKEND_URL;

class CustomerService {
  register(input) {
    return axios
      .post(API_URL + "auths/customers/signup", input)
      .then((response) => {
        return response.data;
      });
  }

  updateProfile(data) {
    return axiosApiInstance.put(API_URL + "customers/info", data);
  }

  getProfile() {
    return axiosApiInstance.get(API_URL + "customers/info");
  }

  getAllUser() {
    return axiosApiInstance.get(API_URL + "list");
  }

  updateAvatar(file) {
    return axiosApiInstance.put(API_URL + "avatar", { file });
  }

  createDeliveryAddress(input) {
    return axiosApiInstance.post(
      API_URL + "customers/delivery-addresses",
      input
    );
  }

  getDeliveryAddress() {
    return axiosApiInstance.get(API_URL + "customers/delivery-addresses");
  }

  getDeliverAddressById(accountId, id) {
    return axiosApiInstance
      .get(API_URL + "customers/" + accountId + "/delivery-addresses/" + id)
      .then((response) => {
        return response.data.payload;
      });
  }

  updateDeliverAddressById(id, data) {
    return axiosApiInstance
      .put(API_URL + "customers/delivery-addresses/" + id, data)
      .then((response) => {
        return response.data.payload;
      });
  }

  deleteDeliverAddress(accountId, id) {
    return axiosApiInstance
      .delete(API_URL + "customers/" + accountId + "/delivery-addresses/" + id)
      .then((response) => {
        return response.data.payload;
      });
  }
}
export default new CustomerService();
