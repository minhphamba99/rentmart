import axiosApiInstance from '../lib/axiosApiInstance';

const API_URL = process.env.BACKEND_URL+ "carts/";
class CartService {
  getMyCarts() {
    return axiosApiInstance.get(API_URL).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }

  updateCart(data) {
      return axiosApiInstance.put(API_URL, data);
  }

  deleteCart(data) {
      return axiosApiInstance.delete(API_URL);
  }

  addToMyCart(data) {
      return axiosApiInstance.post(API_URL, data);
  }
}
export default new CartService();