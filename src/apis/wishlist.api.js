import axiosApiInstance from "../lib/axiosApiInstance";

const API_URL = process.env.BACKEND_URL+ "wishlists/";

class WishlistService {
  getMyWishlists() {
    return axiosApiInstance.get(API_URL).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }

  updateWishlist(data) {
    return axiosApiInstance.update(API_URL + data.id, data);
  }

  deleteWishlist(data) {
    return axiosApiInstance.delete(API_URL + data.id);
  }

  addToMyWishlist(data) {
    return axiosApiInstance.post(API_URL, data);
  }
}
export default new WishlistService();
