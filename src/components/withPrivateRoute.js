import Router from "next/router";
import { AuthToken } from "../apis/auth_token";
import Cookie from "js-cookie";
import React, { Component } from "react";

const login = "/renter/login"; // Define your login route address.

export const privateRoute = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

  hocComponent.getInitialProps = async (context) => {
    var token = context?.req?.headers?.cookie
      ? context?.req?.headers?.cookie
      : `${Cookie.get("refreshToken")}`;
    if (context?.req?.headers?.cookie) {
      token = token.toString().split("refreshToken=")[1];
    }
    const userAuth = new AuthToken(token);
    if (userAuth.isExpired) {
      if (context.res) {
        context.res?.writeHead(302, {
          Location: login,
        });
        context.res?.end();
      } else {
        Router.replace(login);
      }
    } else if (WrappedComponent.getInitialProps) {
      const wrappedProps = await WrappedComponent.getInitialProps({
        ...context,
        auth: userAuth,
      });
      return { ...wrappedProps, userAuth };
    }
    return { userAuth };
  };
  return hocComponent;
};
export const normalRoute = (WrappedComponent) => {
  const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

  hocComponent.getInitialProps = async (context) => {
    const token = context?.req?.headers?.cookie
      ? context?.req?.headers?.cookie
      : `${Cookie.get("refreshToken")}`;
    const userAuth = new AuthToken(token);
    if (userAuth.isAuthenticated) {
      if (context.res) {
        context.res?.writeHead(302, {
          Location: "/",
        });
        context.res?.end();
      } else {
        Router.replace("/");
      }
    } else if (WrappedComponent.getInitialProps) {
      const wrappedProps = await WrappedComponent.getInitialProps({
        ...context,
        auth: userAuth,
      });
      return { ...wrappedProps, userAuth };
    }
    return { userAuth };
  };
  return hocComponent;
};
