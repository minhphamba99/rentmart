import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import accountService from "../../apis/account.api";

export default function ChangePassword({ user, addToast }) {
  const [oldPassword, setOldPassword] = useState();
  const [newPassword, setNewPassword] = useState();
  const [cnfPassword, setCnfPassword] = useState();
  const handleUpdate = async () => {
    if (newPassword !== cnfPassword) {
      addToast("Mật khẩu không trùng khớp", {
        appearance: "warning",
        autoDismiss: true,
      });
    } else {
      await accountService
        .updatePassword({
          oldPassword,
          newPassword,
          confirmPassword: cnfPassword,
        })
        .then(() => {
          addToast("Cập nhật mật khẩu thành công", {
            appearance: "success",
            autoDismiss: true,
          });
        })
        .catch((err) => {
          addToast(err.response.data.status, {
            appearance: "error",
            autoDismiss: true,
          });
        });
    }
  };

  return (
    <div className="account-details-form">
      <form method="post" name="enq">
        <Row>
          <Col className="form-group" md={12}>
            <label htmlFor="oldPassword">
              Mật khẩu cũ <span className="required">*</span>
            </label>
            <input
              required
              className="form-control"
              name="oldPassword"
              type="password"
              onChange={(e) => setOldPassword(e.target.value)}
            />
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="newPassword">
              Mật khẩu mới <span className="required">*</span>
            </label>
            <input
              required
              className="form-control"
              name="newPassword"
              type="password"
              onChange={(e) => setNewPassword(e.target.value)}
            />
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="cnfPassword">
              Nhập lại mật khẩu mới <span className="required">*</span>
            </label>
            <input
              required
              className="form-control"
              name="cnfPassword"
              type="password"
              onChange={(e) => setCnfPassword(e.target.value)}
            />
          </Col>
          <Col className="text-center" md={12}>
            <button
              type="button"
              className="btn btn-fill-out"
              name="button"
              value="button"
              onClick={handleUpdate}
            >
              Cập nhật
            </button>
          </Col>
        </Row>
      </form>
    </div>
  );
}
