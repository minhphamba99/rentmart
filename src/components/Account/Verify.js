import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import accountService from "../../apis/account.api";
import imageService from "../../apis/image.api";
import { fetchProfile } from "../../redux/actions/customerActions";
const Verify = ({ user, addToast, getProfile }) => {
  const isVerify = user.accountId.status;
  const [frontSideImage, setFrontSideImage] = useState({
    file: "",
    imagePreviewUrl: "",
  });
  const [backSideImage, setBackSideImage] = useState({
    file: "",
    imagePreviewUrl: "",
  });
  const [identityNumber, setIdentityNumber] = useState("");
  const [fullName, setFullName] = useState("");
  const [dateOfIssue, setDateOfIssue] = useState("");
  const [placeOfIssue, setPlaceOfIssue] = useState("");
  const [faceImage, setFaceImage] = useState({
    file: "",
    imagePreviewUrl: "",
  });

  const handleFrontImage = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    setFrontSideImage({
      file: file,
      imagePreviewUrl: URL.createObjectURL(file),
    });
  };

  const handleBackImage = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    setBackSideImage({
      file: file,
      imagePreviewUrl: URL.createObjectURL(file),
    });
  };

  const handleFace = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    setFaceImage({
      file: file,
      imagePreviewUrl: URL.createObjectURL(file),
    });
  };

  const handleImage = (name) => {
    document.getElementById(name).click();
  };

  const handleVerify = async () => {
    await imageService
      .postImage([frontSideImage.file, backSideImage.file, faceImage.file])
      .then(async (res) => {
        const images = res;
        await accountService
          .verify({
            fullName,
            identityNumber,
            dateOfIssue,
            placeOfIssue,
            frontSideImage: images[0],
            backSideImage: images[1],
            faceImage: images[2],
          })
          .then((res) => {
            addToast("Xác thực tài khoản thành công", {
              appearance: "success",
              autoDismiss: true,
            });
            getProfile();
          })
          .catch((err) => {
            addToast("Xác thực tài khoản thất bại", {
              appearance: "error",
              autoDismiss: true,
            });
          });
      })
      .catch((err) => {
        addToast("Xác thực tài khoản thất bại", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
  useEffect(() => {
    getProfile();
  }, []);
  return (
    <div className="account-details-form">
      {isVerify === "unverified" && (
        <form method="post" name="enq">
          <Row>
            <Col className="form-group" md={12}>
              <label htmlFor="file-input">
                Họ và tên <span className="required">*</span>
              </label>
              <input
                required
                className="form-control"
                name="fulName"
                type="text"
                onChange={(e) => setFullName(e.target.value)}
              />
            </Col>
            <Col className="form-group" md={12}>
              <label htmlFor="file-input">
                Số CMND/CCCD <span className="required">*</span>
              </label>
              <input
                required
                className="form-control"
                name="idCard"
                type="number"
                maxLength="11"
                onChange={(e) => setIdentityNumber(e.target.value)}
              />
            </Col>
            <Col className="form-group" md={6}>
              <label htmlFor="file-input">
                Ngày cấp <span className="required">*</span>
              </label>
              <input
                required
                className="form-control"
                name="dateCard"
                type="date"
                onChange={(e) => setDateOfIssue(e.target.value)}
              />
            </Col>
            <Col className="form-group" md={6}>
              <label htmlFor="file-input">
                Nơi cấp <span className="required">*</span>
              </label>
              <input
                required
                className="form-control"
                name="location"
                type="text"
                onChange={(e) => setPlaceOfIssue(e.target.value)}
              />
            </Col>
            <Col className="form-group" md={6}>
              <label htmlFor="file-input">
                Ảnh mặt trước <span className="required">*</span>
              </label>
              <div className="image-input">
                <label htmlFor="file-input">
                  <img
                    id="file-image-front"
                    onClick={() => handleImage("file-front")}
                    src={
                      frontSideImage.imagePreviewUrl ||
                      "https://pinkladies24-7.com/assets/images/defaultimg.png"
                    }
                  />
                </label>
                <input
                  id="file-front"
                  type="file"
                  className="write-review__file file-front"
                  onChange={(e) => handleFrontImage(e)}
                />
              </div>
            </Col>
            <Col className="form-group" md={6}>
              <label htmlFor="file-input">
                Ảnh mặt sau <span className="required">*</span>
              </label>
              <div className="image-input">
                <label htmlFor="file-input">
                  <img
                    id="file-image-back"
                    onClick={() => handleImage("file-back")}
                    src={
                      backSideImage.imagePreviewUrl ||
                      "https://pinkladies24-7.com/assets/images/defaultimg.png"
                    }
                  />
                </label>

                <input
                  id="file-back"
                  type="file"
                  className="write-review__file file-back"
                  onChange={(e) => handleBackImage(e)}
                />
              </div>
            </Col>
            <Col className="form-group" md={12}>
              <label htmlFor="file-input">
                Ảnh khuôn mặt của bạn <span className="required">*</span>
              </label>
              <div className="image-input">
                <label htmlFor="file-input">
                  <img
                    id="file-image-face"
                    onClick={() => handleImage("file-face")}
                    src={
                      faceImage.imagePreviewUrl ||
                      "https://pinkladies24-7.com/assets/images/defaultimg.png"
                    }
                  />
                </label>

                <input
                  id="file-face"
                  type="file"
                  className="write-review__file file-face"
                  onChange={(e) => handleFace(e)}
                />
              </div>
            </Col>
            <Col md={12} className="text-center">
              <button
                type="button"
                className="btn btn-fill-out"
                name="button"
                value="button"
                onClick={handleVerify}
              >
                Xác thực
              </button>
            </Col>
          </Row>
        </form>
      )}
      {isVerify === "verifying" && (
        <div className="account-process text-center">
          <h1>Đang chờ xác thực....</h1>
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMk6Iycn9WEfGmwgJnLyFr1C6zhpTi0LGQko8AzZatUfqtwf-sbS-faKOGxXyE-OTl3NE&usqp=CAU" />
          <p> Việc xác thực có thể mất vài ngày</p>
        </div>
      )}
      {isVerify === "verified" && (
        <div className="account-process text-center">
          <img src="https://media.istockphoto.com/vectors/tick-icon-vector-symbol-green-checkmark-isolated-on-white-background-vector-id867053758?k=6&m=867053758&s=170667a&w=0&h=X-PeWPuf2ZkI7mgxlTB4mgyQNvf4C2gI5EPW4FDzvpE=" />
          <h1>Tài khoản đã được xác thực</h1>
        </div>
      )}
    </div>
  );
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfile: fetchProfile,
    },
    dispatch
  );
export default connect(null, mapDispatchToProps)(Verify);
