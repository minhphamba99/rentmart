import React, { useEffect, useState } from "react";
import { Row, Col, InputGroup, FormControl, Form } from "react-bootstrap";
import customerService from "../../apis/customer.api";

export default function Profile({ user, addToast, updateCustomer }) {
  const [name, setName] = useState(user.fullName);
  const [phone, setPhone] = useState(user.phoneNumber);
  const [dob, setDob] = useState(
    new Date(user?.dateOfBirth).toISOString().slice(0, 10)
  );
  const [gender, setGender] = useState(user.gender);
  const isGender = user?.gender === "Nam";

  const handleUpdate = () => {
    const data = {
      fullName: name,
      phoneNumber: phone,
      email: user.email,
      gender: gender,
      dateOfBirth: dob,
      accountId: user.accountId._id,
      _id: user._id,
    };
    updateCustomer(data, addToast);
  };

  return (
    <div className="account-details-form">
      <form method="post" name="enq">
        <Row>
          <Col className="form-group" md={12}>
            <label htmlFor="name">
              Họ tên <span className="required">*</span>
            </label>
            <input
              required
              className="form-control"
              name="name"
              type="text"
              value={name}
              disabled
              // onChange={(e) => setName(e.target.value)}
            />
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="phone">
              Số điện thoại <span className="required">*</span>
            </label>
            <input
              required
              className="form-control"
              type="text"
              name="phone"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="email">Email</label>
            <input
              disabled
              required
              className="form-control"
              name="email"
              type="email"
              value={user?.email}
            />
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="sex">
              Giới tính <span className="required">*</span>
            </label>
            <Form.Group className="mb-0">
              <Form.Check
                className="mx-3"
                inline
                type="radio"
                onChange={(e) => setGender("Nam")}
                defaultChecked={isGender}
                label="Nam"
                name="formHorizontalRadios"
                id="formHorizontalRadios1"
              />
              <Form.Check
                className="mx-3"
                inline
                type="radio"
                onChange={(e) => setGender("Nữ")}
                defaultChecked={!isGender}
                label="Nữ"
                name="formHorizontalRadios"
                id="formHorizontalRadios2"
              />
            </Form.Group>
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="dob">
              Ngày sinh <span className="required">*</span>
            </label>
            <input
              required
              className="form-control w-25"
              name="dob"
              type="date"
              value={dob}
              onChange={(e) => setDob(e.target.value)}
            />
            {/* <Form>
              <Form.Group controlId="exampleForm.SelectCustomSizeLg">
                  <Form.Control className="w-25 mr-5" as="select" size="lg" custom>
                      
                  </Form.Control>
                  <Form.Control className="w-25 mr-5" md={4} as="select" size="lg" custom>
                  </Form.Control>
                  <Form.Control className="w-25 mr-5" md={4} as="select" size="lg" custom>
                  </Form.Control>
              </Form.Group>
          </Form> */}
          </Col>
          <Col className="form-group" md={12}>
            <label htmlFor="dob">
              Điểm số:
              <span className="required ml-2">
                {user?.point?.pointAverage?.toFixed(2) || 5}
              </span>
            </label>
          </Col>
          <Col className="text-center" md={12}>
            <button
              type="button"
              className="btn btn-fill-out"
              name="submit"
              value="Submit"
              onClick={handleUpdate}
            >
              Cập nhật
            </button>
          </Col>
        </Row>
      </form>
    </div>
  );
}
