import { BsStar, BsStarFill } from "react-icons/bs";
import { useState } from "react";
import reviewService from "../../apis/review.api";
import imageService from "../../apis/image.api";
import { useToasts } from "react-toast-notifications";
import { connect } from "react-redux";
const ModalReview = ({ user, orderId, product, agency, setShow }) => {
  const { addToast } = useToasts();
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState("");
  const [rating, setRating] = useState();
  const [title, setTitle] = useState({
    title: "Vui lòng đánh giá",
    placeholder: "Chia sẻ thêm thông tin sản phẩm",
  });
  const [files, setFiles] = useState([]);
  const handleStar = (number) => {
    setRating(number);
    switch (number) {
      case 1:
        setTitle({
          title: "Rất không hài lòng",
          placeholder: "Vấn đề bạn gặp là gì?",
        });
        break;
      case 2:
        setTitle({
          title: "Không hài lòng",
          placeholder: "Vấn đề bạn gặp là gì?",
        });
        break;
      case 3:
        setTitle({
          title: "Bình thường",
          placeholder: "Vấn đề bạn gặp là gì?",
        });
        break;
      case 4:
        setTitle({
          title: "Hài lòng",
          placeholder: "Vấn đề bạn gặp là gì?",
        });
        break;
      case 5:
        setTitle({
          title: "Rất hài lòng",
          placeholder: "Vì sao bạn thích sản phẩm?",
        });
        break;
      default:
        break;
    }
  };
  const handleImage = () => {
    document.getElementById("choose-file").click();
  };
  const readFile = (e) => {
    let file = e.target.files;
    let input = [];
    for (let index = 0; index < file.length; index++) {
      input.push({
        file: file[index],
        imagePreviewUrl: URL.createObjectURL(file[index]),
      });
    }
    setFiles([...files, ...input]);
  };
  const handleReview = async () => {
    let fileImages = [];
    files.map((item) => {
      fileImages.push(item.file);
    });
    if (!content || !rating) {
      addToast("Vui lòng đánh giá, nhập nội dung đánh giá.", {
        appearance: "warning",
        autoDismiss: true,
      });
    } else {
      setLoading(true);
      await imageService
        .postImage(fileImages)
        .then(async (images) => {
          await reviewService
            .reviews(product.productId, {
              avatar:
                user.avatar ||
                "http://res.cloudinary.com/dutvhanvq/image/upload/v…398/Rentmart/564a65d1d650843767ac2ab8b1e68bcc.jpg",
              name: user.fullName,
              content,
              orderId,
              rating,
              images,
            })
            .then((res) => {
              setShow(false);
              setLoading(false);

              addToast("Đánh giá sản phẩm thành công", {
                appearance: "success",
                autoDismiss: true,
              });
              window.location.reload();
            })
            .catch(() => {
              setLoading(false);
              addToast("Đánh giá sản phẩm thất bại", {
                appearance: "error",
                autoDismiss: true,
              });
            });
        })
        .catch(() => {
          setLoading(false);
          addToast("Đánh giá sản phẩm thất bại", {
            appearance: "error",
            autoDismiss: true,
          });
        });
    }
  };

  return (
    <div className="modal-review">
      <div className="modal-review-open">
        <div
          className="modal-review-after-open"
          tabIndex="-1"
          role="dialog"
          aria-label="Viết nhận xét"
        >
          <div className="eeVeyu write-review">
            {!loading && (
              <div
                className="write-review__close"
                onClick={() => setShow(false)}
              >
                ×
              </div>
            )}
            <form className="write-review__inner">
              <div className="write-review__product">
                <img
                  src={product.thumbnail}
                  alt={product.name}
                  className="write-review__product-img"
                />
                <div className="write-review__product-wrap">
                  <div className="write-review__product-name">
                    {product.name}
                  </div>
                  <div className="write-review__product-seller">
                    Cung cấp bởi {agency.agencyName}
                  </div>
                </div>
              </div>
              {!loading && (
                <>
                  <div className="write-review__heading">{title.title}</div>
                  <div className="write-review__stars">
                    <span
                      className="write-review__star"
                      onClick={() => handleStar(1)}
                    >
                      {rating >= 1 ? (
                        <BsStarFill size={36} fill="rgb(255, 181, 0)" />
                      ) : (
                        <BsStar size={36} fill="rgb(255, 181, 0)" />
                      )}
                    </span>
                    <span
                      className="write-review__star"
                      onClick={() => handleStar(2)}
                    >
                      {rating >= 2 ? (
                        <BsStarFill size={36} fill="rgb(255, 181, 0)" />
                      ) : (
                        <BsStar size={36} fill="rgb(255, 181, 0)" />
                      )}
                    </span>
                    <span
                      className="write-review__star"
                      onClick={() => handleStar(3)}
                    >
                      {rating >= 3 ? (
                        <BsStarFill size={36} fill="rgb(255, 181, 0)" />
                      ) : (
                        <BsStar size={36} fill="rgb(255, 181, 0)" />
                      )}
                    </span>
                    <span
                      className="write-review__star"
                      onClick={() => handleStar(4)}
                    >
                      {rating >= 4 ? (
                        <BsStarFill size={36} fill="rgb(255, 181, 0)" />
                      ) : (
                        <BsStar size={36} fill="rgb(255, 181, 0)" />
                      )}
                    </span>
                    <span
                      className="write-review__star"
                      onClick={() => handleStar(5)}
                    >
                      {rating >= 5 ? (
                        <BsStarFill size={36} fill="rgb(255, 181, 0)" />
                      ) : (
                        <BsStar size={36} fill="rgb(255, 181, 0)" />
                      )}
                    </span>
                  </div>
                  <textarea
                    rows="8"
                    placeholder={title.placeholder}
                    className="write-review__input"
                    onChange={(e) => setContent(e.target.value)}
                  ></textarea>
                  <div className="write-review__images">
                    {files?.map((file, key) => {
                      const url = `url(${file.imagePreviewUrl})`;
                      return (
                        <div
                          key={key}
                          className="write-review__image"
                          style={{ backgroundImage: url }}
                        >
                          <div
                            className="write-review__image-close"
                            onClick={() =>
                              setFiles(
                                files.filter(
                                  (item) =>
                                    item.imagePreviewUrl !==
                                    file.imagePreviewUrl
                                )
                              )
                            }
                          >
                            ×
                          </div>
                        </div>
                      );
                    })}
                  </div>
                  <div className="write-review__buttons">
                    <input
                      className="write-review__file"
                      id="choose-file"
                      type="file"
                      multiple
                      onChange={(e) => readFile(e)}
                    />
                    <button
                      type="button"
                      onClick={handleImage}
                      className="write-review__button write-review__button--image"
                    >
                      <img src="https://salt.tikicdn.com/ts/upload/ad/f9/bf/79e0539a981e363ba6ac5bfeef3c70da.png" />
                      <span>Thêm ảnh</span>
                    </button>
                    <button
                      type="button"
                      onClick={handleReview}
                      className="write-review__button write-review__button--submit"
                    >
                      <span>Gửi đánh giá</span>
                    </button>
                  </div>
                </>
              )}
              {loading && <div className="Loader"></div>}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};

export default connect(mapStateToProps)(ModalReview);
