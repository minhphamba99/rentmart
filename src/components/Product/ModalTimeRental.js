import { Fragment, useState } from "react";
import { Modal } from "react-bootstrap";
import * as locales from "react-date-range/dist/locale";
import { DateRange } from "react-date-range";
// import {
//   Datepicker,
//   Input,
//   Page,
//   setOptions,
//   mobiscroll,
//   localeVi,
// } from "../../@mobiscroll/react";
import TextField from "@material-ui/core/TextField";
// setOptions({
//   theme: "ios",
//   themeVariant: "light",
// });

const ModalTimeRental = ({ show, handleClose, handleSave, dateDisable }) => {
  const [time, setTime] = useState([]);
  const [isSelect, setSelect] = useState(false);
  const [startHour, setStartHour] = useState("00:00");
  const [endHour, setEndHour] = useState("00:00");
  const [err, setErr] = useState("");
  const handleSubmit = () => {
    if (!state[0].endDate) {
      setSelect(true);
      setErr("Chưa chọn thời gian thuê");
    } else {
      if (
        state[0].endDate.toString() === state[0].startDate.toString() &&
        startHour >= endHour
      ) {
        setSelect(true);
        setErr("Thời gian không hợp lệ");
      } else {
        handleSave([
          state[0].startDate.toString().replace("00:00:00", startHour),
          state[0].endDate.toString().replace("00:00:00", endHour),
        ]);
        setSelect(false);
        handleClose();
      }
    }
  };
  const [state, setState] = useState([
    {
      startDate: new Date(),
      endDate: null,
      key: "selection",
    },
  ]);
  const handleTime = (timeRange) => {
    setState(timeRange);
    setTime([
      timeRange[0].startDate.toString(),
      timeRange[0].endDate.toString(),
    ]);
  };
  return (
    <Fragment>
      <Modal
        className="modal-time-rental"
        show={show}
        onHide={handleClose}
        animation={false}
      >
        <Modal.Header closeButton>
          <h6 className="modal-title">Chọn thời gian thuê</h6>
        </Modal.Header>
        <Modal.Body>
          <DateRange
            editableDateInputs={true}
            onChange={(item) => handleTime([item.selection])}
            moveRangeOnFirstSelection={false}
            locale={locales["vi"]}
            months={2}
            minDate={new Date()}
            disabledDates={dateDisable}
            direction="horizontal"
            ranges={state}
            dateDisplayFormat="P"
          />
          <div className="row p-3">
            <TextField
              id="time"
              label="Giờ bắt đầu"
              type="time"
              defaultValue="00:00"
              className="col-6 pr-3"
              onChange={(e) => setStartHour(e.target.value)}
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300, // 5 min
              }}
            />
            <TextField
              id="time"
              label="Giờ kết thúc"
              type="time"
              defaultValue="00:00"
              className="col-6 pr-3"
              onChange={(e) => setEndHour(e.target.value)}
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                step: 300, // 5 min
              }}
            />
          </div>
          {/* <Datepicker
            controls={["calendar", "time"]}
            select="range"
            display="inline"
            touchUi={true}
            min={new Date()}
            locale={localeVi}
            theme="windows"
            onChange={(event, inst) => {
              setTime(inst.value);
            }}
          /> */}
        </Modal.Body>
        <Modal.Footer>
          {isSelect && <p style={{ color: "red" }}>{err}</p>}
          <button className="btn btn-fill-out" onClick={handleSubmit}>
            Cập nhật
          </button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default ModalTimeRental;
