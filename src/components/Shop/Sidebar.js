import { Fragment } from "react";
import { IoIosArrowForward } from "react-icons/io";
import Link from "next/link";
import {
  getIndividualCategories,
  getIndividualTags,
  getIndividualColors,
  getProductsIndividualSizes,
  getProducts,
  getDiscountPrice,
  setActiveSort,
} from "../../lib/product";
import { ProductRating } from "../../components/Product";
import { connect } from "react-redux";

const Sidebar = ({ categories, products, getSortParams }) => {
  const categoriesFilter = getIndividualCategories(categories, products);
  const colors = getIndividualColors(products);
  const sizes = getProductsIndividualSizes(products);
  const tags = getIndividualTags(products);
  const popularProducts = getProducts(products, "fashion", "popular", 3);

  return (
    <div className="sidebar">
      <div className="widget">
        <h5 className="widget__title">Danh mục</h5>
        {categoriesFilter.length > 0 ? (
          <ul className="widget__categories">
            {categoriesFilter &&
              categoriesFilter.map((category, key) => {
                return (
                  <li key={key}>
                    <Link
                      href={{
                        pathname: "/[category]",
                        query: {
                          category: `${category?.name.replaceAll(" ", "-")}`,
                          stg: category._id,
                        },
                      }}
                    >
                      <a
                      // onClick={(e) => {
                      //   getSortParams("category", category.name);
                      //   setActiveSort(e);
                      // }}
                      >
                        <IoIosArrowForward />
                        <span className="categories-name">{category.name}</span>
                        {/* <span className="categories-num">({category.count})</span> */}
                      </a>
                    </Link>
                  </li>
                );
              })}
          </ul>
        ) : (
          "Không tìm thấy danh mục "
        )}
      </div>

      <div className="widget">
        <h5 className="widget__title">Đánh giá</h5>
        <ul className="widget__sizes">
          <li>
            <button onClick={() => console.log("5")}>
              <ProductRating ratingValue={5} />
              <span>Từ 5 sao</span>
            </button>
          </li>
          <li>
            <button onClick={() => console.log("4")}>
              <ProductRating ratingValue={4} />
              <span>Từ 4 sao</span>
            </button>
          </li>
          <li>
            <button onClick={() => console.log("3")}>
              <ProductRating ratingValue={3} />
              <span>Từ 3 sao</span>
            </button>
          </li>
        </ul>
      </div>

      <div className="widget">
        <h5 className="widget__title">Giá</h5>
        {categoriesFilter.length > 0 ? (
          <ul className="widget__colors">
            {colors.map((color, key) => {
              return (
                <li key={key}>
                  <button
                    onClick={(e) => {
                      getSortParams("color", color.colorName);
                      setActiveSort(e);
                    }}
                    style={{ backgroundColor: color.colorCode }}
                  ></button>
                </li>
              );
            })}
            <li>
              <button
                onClick={(e) => {
                  getSortParams("color", "");
                  setActiveSort(e);
                }}
              >
                x
              </button>
            </li>
          </ul>
        ) : (
          ""
        )}
      </div>

      <div className="widget">
        <h5 className="widget__title">Sản phẩm phổ biến</h5>
        {popularProducts?.length > 0 ? (
          <ul className="widget-recent-post-wrapper">
            {popularProducts &&
              popularProducts.map((product, key) => {
                const discountedPrice = getDiscountPrice(
                  product.price,
                  product.discount
                ).toFixed(2);
                const productPrice = product.price.toFixed(2);
                return (
                  <li className="widget-product-post" key={key}>
                    <div className="widget-product-post__image">
                      <Link
                        href={{
                          pathname: "/product/[slug]",
                          query: {
                            slug: `${product.name.replaceAll(" ", "-")}`,
                            spid: product._id,
                          },
                        }}
                      >
                        <a>
                          <img src={product.thumbImage[0]} alt="shop_small1" />
                        </a>
                      </Link>
                    </div>
                    <div className="widget-product-post__content">
                      <h6 className="product-title">
                        <Link
                          href={{
                            pathname: "/product/[slug]",
                            query: {
                              slug: `${product.name.replaceAll(" ", "-")}`,
                              spid: product._id,
                            },
                          }}
                        >
                          <a>{product.name}</a>
                        </Link>
                      </h6>
                      <div className="product-price">
                        {product.discount ? (
                          <Fragment>
                            <span className="price">${discountedPrice}</span>
                            <del>${formatPrice(productPrice)}</del>
                          </Fragment>
                        ) : (
                          <span className="price">
                            {formatPrice(productPrice)}&nbsp;₫
                          </span>
                        )}
                      </div>
                      <div className="rating-wrap">
                        <ProductRating ratingValue={product.rate.rateAverage} />
                      </div>
                    </div>
                  </li>
                );
              })}
          </ul>
        ) : (
          "No products found"
        )}
      </div>

      <div className="widget">
        <h5 className="widget__title">tags</h5>
        {tags.length > 0 ? (
          <div className="widget__tags">
            {tags &&
              tags.map((tag, key) => {
                return (
                  <button
                    key={key}
                    onClick={(e) => {
                      getSortParams("tag", tag);
                      setActiveSort(e);
                    }}
                  >
                    {tag}
                  </button>
                );
              })}
          </div>
        ) : (
          "Không có từ khoá nào"
        )}
      </div>

      <div className="widget">
        <div className="shop-banner">
          <div className="banner-img">
            <img
              src="/assets/images/banner/sidebar_banner_img.jpg"
              alt="sidebar_banner_img"
            />
          </div>
          <div className="shop-bn-content2">
            <h5 className="text-uppercase shop-subtitle">New Collection</h5>
            <h3 className="text-uppercase shop-title">Sale 30% Off</h3>
            <Link href="/[category]?category=may-tinh" as="/may-tinh/?spm=1">
              <a className="btn btn-white rounded-0 btn-sm text-uppercase">
                Shop Now
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    categories: state.categoryData.categories,
  };
};
export default connect(mapStateToProps)(Sidebar);
