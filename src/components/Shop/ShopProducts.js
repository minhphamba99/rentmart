import { FaBookmark } from "react-icons/fa";
import Link from "next/link";
import { formatPrice } from "../../lib/utilities";
import { getIndividualCategories } from "../../lib/product";

const ShopProducts = ({ categories, shop, trendingProducts }) => {
  const productsTrend = trendingProducts.products;
  const cat = getIndividualCategories(
    categories,
    trendingProducts.products
  ).slice(0, 3);

  return (
    <>
      <div className="store-products-categories epfiby">
        <div className="fPxkFE">Mua sắm theo danh mục</div>
        <div className="categories drQiqr">
          {cat?.map((category, index) => (
            <div key={index} className="categories__item byEdcD efsPJP">
              <div className="categories__item__title jwRAbm">
                {category.name}
              </div>
              <div className="categories__item__img bHyxSX">
                <img
                  src={category.icon}
                  alt="category thumbnail"
                  className="Categories__Image-pswkmo-4 hkPPDv"
                />
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="store-products-trend epfiby">
        <div className="ProductRow__Wrapper-sc-13ncpqb-0 lfDTCq">
          <div className="WidgetTitle__Title-e2wjwt-0 fPxkFE">
            Thuê nhiều nhất
            <Link
              href={{
                pathname: "/shop/[shop]",
                query: {
                  shop: `${shop.agencyName.replaceAll(" ", "-")}`,
                  sid: shop.accountId._id,
                  t: "products",
                },
              }}
            >
              <a
                target="_blank"
                className="WidgetTitle__ViewAll-e2wjwt-1 feWWnT"
              >
                Xem tất cả
              </a>
            </Link>
          </div>
          <div
            type="flex"
            className="ProductRow__Container-sc-13ncpqb-1 ilyIrF"
            style={{
              backgroundColor: "transparent",
              margin: "0px -10px",
              flexWrap: "nowrap",
            }}
          >
            {productsTrend.map((product, index) => (
              <Link
                key={product._id}
                href={{
                  pathname: "/product/[slug]",
                  query: {
                    slug: `${product.name.replaceAll(" ", "-")}`,
                    spid: product._id,
                  },
                }}
              >
                <a
                  className="ProductBestSelling__Wrapper-sc-49dzjv-0 lgAVmE"
                  style={{ width: "33.3333%" }}
                >
                  <div className="ProductBestSelling__Container-sc-49dzjv-1 gAJuvp">
                    <span className="ProductBestSelling__Badge-sc-49dzjv-2 BMDBU">
                      <FaBookmark />
                      <span className="ProductBestSelling__BadgeValue-sc-49dzjv-3 eDrkRY">
                        {index + 1}
                      </span>
                    </span>
                    <div className="ProductBestSelling__Thumbnail-sc-49dzjv-4 CYqzz">
                      <img
                        src={product.thumbnail}
                        // alt="Kéo Thép Đa Năng Nhật Bản, Size Lớn, Chất Liệu Thép Cao Cấp Không Gỉ"
                        className="ProductBestSelling__ThumbnailImg-sc-49dzjv-5 fpodVc"
                      />
                    </div>
                    <div className="ProductBestSelling__Info-sc-49dzjv-6 gciGrt">
                      <div className="ProductBestSelling__Name-sc-49dzjv-7 jVWQUC">
                        <span>{product.name}</span>
                      </div>
                      <div className="ProductBestSelling__Price-sc-49dzjv-8 ctYvBX">
                        <span>
                          {formatPrice(product.rentPrice)}&nbsp;₫ /
                          {product.rentDetail.unitRent === "date"
                            ? "Ngày"
                            : "Giờ"}
                        </span>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ShopProducts;
