import { FaRegCalendarCheck, FaBoxOpen } from "react-icons/fa";
import { AiOutlineShop } from "react-icons/ai";
import { RiUserFollowLine } from "react-icons/ri";
const ShopInfo = ({ shop, totalProducts }) => {
  return (
    <div className=" store_info StoreInfo__Row-sc-1un24du-0 StoreInfo__Wrapper-sc-1un24du-7 iZsHWg">
      <div className="StoreInfo__Col-sc-1un24du-1 StoreInfo__ScoreWrapper-sc-1un24du-6 ttJQZ">
        <div
          span="6"
          className="Score__Col-wpqkxv-0 Score__ScoreWrapper-wpqkxv-2 gEOlFQ"
        >
          <div
            className="Score__Row-wpqkxv-5 gHZKeE"
            style={{ paddingBottom: "0px", paddingLeft: "0px" }}
          >
            <span className="Score__TooltipText-wpqkxv-3 iQtjKm">
              Tỉ lệ huỷ
            </span>
            <div
              data-type="info"
              data-tip="Tỷ lệ đơn hàng huỷ do lỗi người bán trong 30 ngày qua"
              className="Score__TooltipIcon-wpqkxv-4 bMreQR form__tooltip"
              data-for="cancel"
              currentitem="false"
            >
              <img
                width="12px"
                src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/info.svg"
              />
            </div>
          </div>
          <div className="Score__StyledToolTip-wpqkxv-6 eIsBnP">
            <div
              className="__react_component_tooltip place-bottom type-info tooltip"
              id="cancel"
              data-id="tooltip"
              style={{ left: "358px", top: "348px" }}
            >
              Tỷ lệ đơn hàng huỷ do lỗi người bán trong 30 ngày qua
            </div>
          </div>
          <div className="Score__ScoreText-wpqkxv-1 gDlbsW">0 %</div>
        </div>
        <div
          span="6"
          className="Score__Col-wpqkxv-0 Score__ScoreWrapper-wpqkxv-2 gEOlFQ"
        >
          <div
            className="Score__Row-wpqkxv-5 gHZKeE"
            style={{ paddingBottom: "0px", paddingLeft: "0px" }}
          >
            <span className="Score__TooltipText-wpqkxv-3 iQtjKm">
              Tỉ lệ đổi trả
            </span>
            <div
              data-type="info"
              data-tip="Tỷ lệ đơn hàng đổi/trả do lỗi người bán trong 30 ngày qua"
              className="Score__TooltipIcon-wpqkxv-4 bMreQR form__tooltip"
              data-for="return"
              currentitem="false"
            >
              <img
                width="12px"
                src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/info.svg"
              />
            </div>
          </div>
          <div className="Score__StyledToolTip-wpqkxv-6 eIsBnP">
            <div
              className="__react_component_tooltip place-bottom type-dark"
              id="return"
              data-id="tooltip"
            ></div>
          </div>
          <div className="Score__ScoreText-wpqkxv-1 gDlbsW">0 %</div>
        </div>
      </div>
      <div className="StoreInfo__Row-sc-1un24du-0 StoreInfo__InfoWrapper-sc-1un24du-2 lePLBy">
        <div className="StoreInfo__Col-sc-1un24du-1 StoreInfo__InfoCol-sc-1un24du-3 jdBoVi">
          <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
            <FaRegCalendarCheck />
            <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
              Thành viên từ năm
            </span>
          </span>
          <span className="StoreInfo__InfoValue-sc-1un24du-5 eRKPPD">
            {new Date(shop.accountId.createdAt).getFullYear()}
          </span>
        </div>
        <div className="StoreInfo__Col-sc-1un24du-1 StoreInfo__InfoCol-sc-1un24du-3 jdBoVi">
          <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
            <FaBoxOpen />
            <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
              Sản phẩm
            </span>
          </span>
          <span className="StoreInfo__InfoValue-sc-1un24du-5 eRKPPD">
            {totalProducts}+
          </span>
        </div>
        <div className="StoreInfo__Col-sc-1un24du-1 StoreInfo__InfoCol-sc-1un24du-3 jdBoVi">
          <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
            <AiOutlineShop />
            <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
              Mô tả cửa hàng
            </span>
          </span>
          <span className="StoreInfo__InfoValue-sc-1un24du-5 eRKPPD">
            {shop.about}
          </span>
        </div>
        <div className="StoreInfo__Col-sc-1un24du-1 StoreInfo__InfoCol-sc-1un24du-3 jdBoVi">
          <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
            <RiUserFollowLine />
            <span className="StoreInfo__InfoLabel-sc-1un24du-4 gVnWsx">
              Người theo dõi
            </span>
          </span>
          <span className="StoreInfo__InfoValue-sc-1un24du-5 eRKPPD">147</span>
        </div>
      </div>
    </div>
  );
};

export default ShopInfo;
