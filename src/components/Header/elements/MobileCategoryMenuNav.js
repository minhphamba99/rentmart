import { useEffect } from "react";
import Link from "next/link";

const MobileCategoryMenuNav = ({ getActiveStatus }) => {
  useEffect(() => {
    const offCanvasNav = document.querySelector(
      "#offcanvas-mobile-category-menu__navigation"
    );
    const offCanvasNavSubMenu = offCanvasNav.querySelectorAll(
      ".mobile-sub-menu"
    );
    const anchorLinks = offCanvasNav.querySelectorAll("a");

    for (let i = 0; i < offCanvasNavSubMenu.length; i++) {
      offCanvasNavSubMenu[i].insertAdjacentHTML(
        "beforebegin",
        "<span class='menu-expand'><i></i></span>"
      );
    }

    const menuExpand = offCanvasNav.querySelectorAll(".menu-expand");
    const numMenuExpand = menuExpand.length;

    for (let i = 0; i < numMenuExpand; i++) {
      menuExpand[i].addEventListener("click", (e) => {
        sideMenuExpand(e);
      });
    }

    for (let i = 0; i < anchorLinks.length; i++) {
      anchorLinks[i].addEventListener("click", () => {
        getActiveStatus(false);
      });
    }
  });

  const sideMenuExpand = (e) => {
    e.currentTarget.parentElement.classList.toggle("active");
  };
  return (
    <nav
      className="offcanvas-mobile-menu__navigation space-mb--30"
      id="offcanvas-mobile-category-menu__navigation"
    >
      <ul>
        <li className="menu-item-has-children">
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Woman's</a>
          </Link>
          <ul className="mobile-sub-menu">
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Featured Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Popular Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li className="menu-item-has-children">
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Man's</a>
          </Link>
          <ul className="mobile-sub-menu">
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Featured Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Popular Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>New Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li className="menu-item-has-children">
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Kid's</a>
          </Link>
          <ul className="mobile-sub-menu">
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Featured Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="menu-item-has-children">
              <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                <a>Popular Item</a>
              </Link>
              <ul className="mobile-sub-menu">
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Curabitur tempus</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vivamus in tortor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae ante ante</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Etiam ac rutrum</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Vestibulum sed</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec porttitor</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Donec vitae facilisis</a>
                  </Link>
                </li>
                <li>
                  <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
                    <a>Quisque condimentum</a>
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </li>

        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Accessories</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Clothing</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Shoes</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Watches</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Jewelry</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Health & Beauty</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Sports</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Sleepwear</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Seasonal Wear</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Ethnic Wear</a>
          </Link>
        </li>
        <li>
          <Link href="/[category]?category=may-tinh"
                    as="/may-tinh/?spm=1">
            <a>Baby Clothing</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default MobileCategoryMenuNav;
