import Link from "next/link";
import { Container, Row, Col, Form } from "react-bootstrap";
import {
  IoIosPhonePortrait,
  IoIosShuffle,
  IoIosHeartEmpty
} from "react-icons/io";
import { AiOutlineUser } from "react-icons/ai";
import { FaApple, FaAndroid,BsPeopleCircle } from "react-icons/fa";

const HeaderTop = ({ containerClass }) => {
  return (
    <div className="top-header d-none d-lg-block bg--dark light-skin">
        <div  className={`${containerClass ? containerClass : "custom-container"}`} >
        
          <Row className="align-items-center">
            <Col md={6}>
              <div className="header-topbar-info d-flex">
                <div className="header-offer">
                  <span>Free Ground Shipping Over $250</span>
                </div>
                <div className="download-wrap">
                  <span className="mr-3">Tải ứng dụng trên điện thoại</span>
                  <ul className="text-center text-lg-left">
                    <li>
                      <a href="#">
                        <FaApple />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <FaAndroid />
                      </a>
                    </li>
                  </ul>
                </div>
                {/* <ul className="contact-detail text-center text-lg-left">
                    <li>
                      <IoIosPhonePortrait />
                      <span>123-456-7890</span>
                    </li>
                  </ul> */}
              </div>
            </Col>
            <Col md={6}>
              <div className="text-center text-md-right header-topbar-info">
                <ul className="header-list">
                  <li>
                    <Link href="/other/compare">
                      <a>
                        <IoIosShuffle />
                        <span>So sánh</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/other/wishlist">
                      <a>
                        <IoIosHeartEmpty />
                        <span>Danh sách yêu thích</span>
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href="/renter/login">
                      <a>
                        <AiOutlineUser />
                        <span>Đăng nhập</span>
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </div>
    </div>
  );
};

export default HeaderTop;
