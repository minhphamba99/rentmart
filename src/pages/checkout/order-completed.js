import Link from "next/link";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { Container, Row, Col } from "react-bootstrap";
import { IoIosCheckmarkCircle } from "react-icons/io";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCarts } from "../../redux/actions/cartActions";
import { useEffect } from "react";

const OrderCompleted = ({ getCarts }) => {
  const router = useRouter();
  console.log("thanh toán", router.query);
  const res = router.query;
  useEffect(() => {
    getCarts();
  }, []);
  return (
    <LayoutSix>
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Order Completed">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">
            {res.errorCode !== "0" ? "Thuê thất bại" : "Thuê thành công"}
          </li>
        </ol>
      </BreadcrumbOne>
      <div className="order-content space-pt--20 space-pb--r100">
        <Container>
          <Row className="justify-content-center">
            <Col md={8}>
              <div className="text-center order-complete">
                <IoIosCheckmarkCircle />
                <div className="heading-s1 space-mb--20">
                  <h3>{res.localMessage || "Đơn hàng của bạn đã hoàn tất!"}</h3>
                </div>
                {res.errorCode !== "0" ? (
                  <p>
                    Đơn hàng của bạn thanh toán không thành công. Vui lòng thanh
                    toán lại!
                  </p>
                ) : (
                  <p>
                    Cảm ơn bạn đã đặt hàng của bạn! Đơn hàng của bạn đang được
                    xử lý và sẽ hoàn thành trong vòng 3-6 giờ. Bạn sẽ nhận được
                    email xác nhận khi đơn hàng của bạn hoàn tất.
                  </p>
                )}
                <Link href="/">
                  <a className="btn btn-fill-out">Tiếp tục thuê hàng</a>
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </LayoutSix>
  );
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getCarts: fetchCarts,
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(OrderCompleted);
