import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { IoMdCash } from "react-icons/io";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import paymentService from "../../apis/payment.api";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { logoutCart } from "../../redux/actions/cartActions";
import { bindActionCreators } from "redux";
import { privateRoute } from "../../components/withPrivateRoute";
import { useState } from "react";
import { useToasts } from "react-toast-notifications";
import { OrderInfo, PaymentMethod } from "../../components/Payment";

const Checkout = ({ user, cartItems, handleLogoutCart }) => {
  const { addToast } = useToasts();

  const router = useRouter();
  const [orders, setOrders] = useState(null);
  const [delivery, setDelivery] = useState({
    fullName: "",
    address: null,
    phoneNumber: "",
  });
  const [method, setMethod] = useState("");
  const [methodDefault, setMethodDefault] = useState("");
  useEffect(() => {
    async function getCheckout() {
      await paymentService.getPaymentInfo().then((data) => {
        if (data.products.length > 0) {
          console.log(data)
          setOrders(data);
          setDelivery({
            address: data.products[0].deliveryAddress,
            fullName: data.products[0].fullName,
            phoneNumber: data.products[0].phoneNumber,
          });
          setMethod(data.products[0].payment.method);
          setMethodDefault(data.products[0].payment.method);
        }
      });
    }
    getCheckout();
  }, []);
  const handlePaymentMethod = async (e) => {
    const value = e.target.value;
    setMethod(value);
  };
  const payment = async () => {
    if (user.accountId.status !== "verified") {
      addToast(
        "Tài khoản bạn chưa được xác thực. Vui lòng chờ xác thực để đặt hàng.",
        {
          appearance: "warning",
          autoDismiss: true,
        }
      );
      return;
    }
    if (!delivery.address) {
      addToast("Bạn chưa có địa chỉ nhận hàng. Vui lòng tạo địa chỉ.", {
        appearance: "warning",
        autoDismiss: true,
      });
      return;
    }

    if (method) {
      const returnUrl = "https://rentmart.vercel.app/checkout/order-completed";
      // const returnUrl = "http://localhost:3000/checkout/order-completed";
      if (method !== methodDefault) {
        await paymentService.putPaymentMethod(method).then(async (res) => {
          await paymentService
            .checkout(returnUrl)
            .then((res) => {
              if (method === "cash") {
                router.push("/checkout/order-completed");
                handleLogoutCart();
              } else {
                const data = JSON.parse(res);
                window.location.replace(data.payUrl);
              }
            })
            .catch((err) => {});
        });
      } else {
        await paymentService
          .checkout(returnUrl)
          .then((res) => {
            if (method === "cash") {
              // router.push("/checkout/order-completed");
              router.push({
                pathname: "/checkout/order-completed",
                query: {
                  localMessage: "",
                  errorCode: "0",
                },
              });
            } else {
              const data = JSON.parse(res);
              window.location.replace(data.payUrl);
            }
          })
          .catch((err) => {
            router.push({
              pathname: "/checkout/order-completed",
              query: {
                localMessage: "Đơn hàng đã bị huỷ bỏ",
                errorCode: "49",
              },
            });
          });
      }
    } else {
      addToast("Vui lòng chọn phương thức thanh toán", {
        appearance: "warning",
        autoDismiss: true,
      });
    }
  };
  return (
    <LayoutSix title="Thông tin thanh toán | Rent Mart">
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Checkout">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">Thanh toán</li>
        </ol>
      </BreadcrumbOne>
      <div className="checkout-content payment space-pt--20 space-pb--r100">
        <Container>
          {cartItems && cartItems.length >= 1 ? (
            <Row>
              <Col md={8}>
                <PaymentMethod
                  delivery={delivery}
                  orders={orders}
                  handlePaymentMethod={handlePaymentMethod}
                  payment={payment}
                  method={method}
                />
              </Col>
              <Col md={4}>
                <OrderInfo delivery={delivery} orders={orders} />
              </Col>
            </Row>
          ) : (
            <Row>
              <Col>
                <div className="item-empty-area text-center">
                  <div className="item-empty-area__icon space-mb--30">
                    <IoMdCash />
                  </div>
                  <div className="item-empty-area__text">
                    <p className="space-mb--30">
                      Không có mặt hàng nào được tìm thấy trong giỏ hàng để
                      thanh toán
                    </p>
                    <Link href="/">
                      <a className="btn btn-fill-out">Thuê ngay</a>
                    </Link>
                  </div>
                </div>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </LayoutSix>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartData,
    user: state.userData.user,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      handleLogoutCart: logoutCart,
    },
    dispatch
  );
export default privateRoute(
  connect(mapStateToProps, mapDispatchToProps)(Checkout)
);
