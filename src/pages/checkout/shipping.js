import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { IoMdCash } from "react-icons/io";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { logoutCart } from "../../redux/actions/cartActions";
import { bindActionCreators } from "redux";
import { privateRoute } from "../../components/withPrivateRoute";
import { useState } from "react";
import { useToasts } from "react-toast-notifications";
import customerService from "../../apis/customer.api";
import CardLoad from "../../components/Preloader/cardLoad";
import Address from "../../components/Account/Address";
const Shipping = ({ user, cartItems, handleLogoutCart }) => {
  const [addressList, setAddressList] = useState([]);
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  useEffect(() => {
    async function getDeliveryAddress() {
      await customerService
        .getDeliveryAddress()
        .then((response) => {
          setLoading(false);
          setAddressList(response.data.payload);
        })
        .catch();
    }
    getDeliveryAddress();
  }, []);
  const { addToast } = useToasts();

  const handleDelete = async (id) => {
    await customerService
      .deleteDeliverAddress(user._id, id)
      .then(() => {
        setAddressList(addressList.filter((item) => item._id !== id));
        addToast("Xoá địa chỉ thành công", {
          appearance: "success",
          autoDismiss: true,
        });
      })
      .catch(() => {
        addToast("Xoá địa chỉ thất bại", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
  const updateAddress = async (address) => {
    const input = {
      fullName: address.fullName,
      phoneNumber: address.phoneNumber,
      selected: true,
      streetAndNumber: address.streetAndNumber,
      wardName: address.wardName,
      wardCode: address.wardCode,
      wardPrefix: address.wardPrefix,
      districtName: address.districtName,
      districtCode: address.districtCode,
      districtPrefix: address.districtPrefix,
      cityName: address.cityName,
      cityCode: address.cityCode,
      cityPrefix: address.cityPrefix,
    };
    await customerService
      .updateDeliverAddressById(address._id, input)
      .then(() => {
        addToast("Cập nhật địa chỉ thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        router.push("/checkout/payment");
      })
      .catch((error) => {
        addToast("Cập nhật địa chỉ thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
  return (
    <LayoutSix title="Thông tin giao hàng | Rent Mart">
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="shipping">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">Địa chỉ giao hàng</li>
        </ol>
      </BreadcrumbOne>
      <div className="checkout-content payment space-pt--20 space-pb--r100">
        <Container className="shipping">
          <div className="jNYLsd">
            <h5 className="address-list-text">
              Chọn địa chỉ giao hàng có sẵn bên dưới:
            </h5>
            <div className="address-list">
              {addressList.length > 0 &&
                addressList.map((address, index) => (
                  <div className="hheNUT" key={index}>
                    <p className="name">{address.fullName}</p>
                    <p
                      className="address"
                      title="6/3 , kp: Đông Tân, Phường Dĩ An, Thị xã Dĩ An, Bình Dương"
                    >
                      Địa chỉ: {address.streetAndNumber} , {address.wardName},{" "}
                      {address.districtName}, {address.cityName}
                    </p>
                    <p className="address">Việt Nam</p>
                    <p className="phone">Điện thoại: {address.phoneNumber}</p>
                    <p className="action">
                      <button
                        type="button"
                        onClick={() => updateAddress(address)}
                        className={`btn saving-address ${
                          !address.selected ? "" : "select"
                        }`}
                      >
                        Giao đến địa chỉ này
                      </button>
                      <button
                        onClick={() =>
                          router.push({
                            pathname: "/renter/address/create",
                            query: {
                              address: "edit",
                              aid: address._id,
                            },
                          })
                        }
                        type="button"
                        className="btn edit-address"
                      >
                        {" "}
                        Sửa
                      </button>
                      {address.selected || (
                        <button
                          onClick={() => handleDelete(address._id)}
                          type="button"
                          className="btn delete-address"
                        >
                          {" "}
                          Xoá
                        </button>
                      )}
                    </p>
                    {!address.selected || (
                      <span className="default">Mặc định</span>
                    )}
                  </div>
                ))}
              {!loading && addressList.length === 0 && (
                <p>Bạn chưa có địa chỉ nào.</p>
              )}
              {loading && <CardLoad />}
            </div>
          </div>
          <p className="gPtlxm">
            Bạn muốn giao hàng đến địa chỉ khác?{" "}
            <span onClick={() => router.push("/renter/address/create")}>
              Thêm địa chỉ giao hàng mới
            </span>
          </p>
        </Container>
      </div>
    </LayoutSix>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartData,
    user: state.userData.user,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      handleLogoutCart: logoutCart,
    },
    dispatch
  );
export default privateRoute(
  connect(mapStateToProps, mapDispatchToProps)(Shipping)
);
