import Link from "next/link";
import { LayoutSix } from "../layouts";
import { BreadcrumbOne } from "../components/Breadcrumb";
import { Container, Row, Col } from "react-bootstrap";
import { IoIosSearch } from "react-icons/io";

const NotFound = () => {
  return (
    <LayoutSix>
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Not Found">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">Không tìm thấy</li>
        </ol>
      </BreadcrumbOne>
      <div className="not-found-content space-pt--20 space-pb--r100">
        <Container>
          <Row className="align-items-center justify-content-center">
            <Col lg={6} md={10}>
              <div className="text-center">
                <div className="error-txt">404</div>
                <h5 className="mb-2 mb-sm-3">
                  oops! Trang bạn yêu cầu không tìm thấy!
                </h5>
                <p>
                  Trang bạn đang tìm kiếm đã bị di chuyển, xóa, đổi tên hoặc
                  có thể không bao giờ tồn tại. 
                </p>
                <div className="search-form pb-3 pb-md-4">
                  <form method="post">
                    <input
                      name="text"
                      id="text"
                      type="text"
                      placeholder="Search"
                      className="form-control"
                    />
                    <button type="submit" className="btn icon-search">
                      <IoIosSearch />
                    </button>
                  </form>
                </div>
                <Link href="/">
                  <a className="btn btn-fill-out">Trang chủ</a>
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </LayoutSix>
  );
};

export default NotFound;
