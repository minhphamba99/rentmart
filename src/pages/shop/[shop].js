import React, { useState, useEffect, Fragment } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import Paginator from "react-hooks-paginator";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import {
  Sidebar,
  OptionHeader,
  GridProducts,
  ShopProducts,
  ShopHeader,
  ShopInfo,
} from "../../components/Shop";
import { getSortedProducts } from "../../lib/product";
import {
  fetchProductsCategory,
  fetchProducts,
} from "../../redux/actions/productActions";
import { useRouter } from "next/router";
import { bindActionCreators } from "redux";
import shopService from "../../apis/shop.api";
import productService from "../../apis/product.api";

const Shop = ({ categories, shop, data, query, trendingProducts }) => {
  const products = data?.products;
  const router = useRouter();
  const [layout, setLayout] = useState("grid");
  const getLayout = (layout) => {
    setLayout(layout);
  };
  const isHome = query.t === "store" || !query.t;
  const isProducts = query.t === "products";
  const isInfo = query.t === "info";
  return (
    <LayoutSix title={`${shop.agencyName} | Rent Mart`}>
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Shop">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">{shop.agencyName}</li>
        </ol>
      </BreadcrumbOne>
      <div className=" shop-content shop-info-content space-pb--r100">
        <Container>
          <ShopHeader shop={shop} query={query} />
          {!isHome || (
            <Fragment>
              <ShopProducts
                shop={shop}
                categories={categories}
                trendingProducts={trendingProducts}
              />
              <OptionHeader
                getLayout={getLayout}
                // getFilterSortParams={getFilterSortParams}
                // shopTopFilterStatus={shopTopFilterStatus}
                // setShopTopFilterStatus={setShopTopFilterStatus}
                layout={layout}
              />
              <GridProducts layout="grid" products={products} />
            </Fragment>
          )}
          {!isProducts || (
            <Row className="store-products mt-3">
              <Col className="grid-products" lg={9}>
                {/* shop page header */}
                <h5 className="my-3">
                  {" "}
                  Tất cả sản phẩm: <span>{data.totalProducts} sản phẩm</span>
                </h5>
                <OptionHeader
                  getLayout={getLayout}
                  // getFilterSortParams={getFilterSortParams}
                  // shopTopFilterStatus={shopTopFilterStatus}
                  // setShopTopFilterStatus={setShopTopFilterStatus}
                  layout={layout}
                />
                {/* shop products */}
                <GridProducts layout={layout} products={products} />
                {/* shop product pagination */}
                <div className="pagination pagination-style pagination-style--two justify-content-center">
                  {/* <Paginator
                  totalRecords={sortedProducts.length}
                  pageLimit={pageLimit}
                  pageNeighbours={1}
                  setOffset={setOffset}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
                  pageContainerClass="mb-0 mt-0"
                  pagePrevText="«"
                  pageNextText="»"
                /> */}
                </div>
              </Col>
              <Col lg={3} className="order-lg-first mt-4 pt-2 mt-lg-0 pt-lg-0">
                {/* sidebar */}
                <Sidebar products={products} />
              </Col>
            </Row>
          )}
          {!isInfo || (
            <ShopInfo shop={shop} totalProducts={data.totalProducts} />
          )}
        </Container>
      </div>
    </LayoutSix>
  );
};

const mapStateToProps = (state) => {
  return {
    categories: state.categoryData.categories,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setProducts: fetchProducts,
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(Shop);

export async function getServerSideProps({ query }) {
  try {
    const shop = await shopService.getShopDetail(query.sid);
    const trendingProducts = await productService.getProducts({
      agencyId: query.sid,
      sort: "rentCount: -1",
      limit: 3,
    });
    const data = await productService.getProducts({
      agencyId: query.sid,
      keyword: query.q,
      sort: "rentCount: -1",
      limit: 20,
      page: query.page,
    });
    return { props: { shop, data, query, trendingProducts } };
  } catch {
    return {
      redirect: {
        destination: "/not-found",
        permanent: false,
      },
    };
  }
}
