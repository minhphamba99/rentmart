import Link from "next/link";
import { useState } from "react";
import { LayoutSix } from "../../layouts";
import { useRouter } from "next/router";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { Container, Row, Col, Form } from "react-bootstrap";
import { FaFacebookF, FaGooglePlusG } from "react-icons/fa";
import customerService from "../../apis/customer.api";
import accountService from "../../apis/account.api";
import { normalRoute } from "../../components/withPrivateRoute";

const Register = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [dob, setDob] = useState("");
  const [phone, setPhone] = useState("");
  const [name, setName] = useState("");
  const [gender, setGender] = useState("Nam");

  const register = async () => {
    if (password === confirmPassword) {
      await customerService
        .register({
          email,
          password,
          dateOfBirth: dob,
          phoneNumber: phone,
          fullName: name,
          gender,
          confirmPassword,
        })
        .then(async (response) => {
          await accountService.login({
            email,
            password,
          });
          router.push("/");
        });
    }
  };

  return (
    <LayoutSix>
      {/* breadcrumb */}
      {/* <BreadcrumbOne pageTitle="Register">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">Register</li>
        </ol>
      </BreadcrumbOne> */}
      <div className="login-content space-pt--20 space-pb--r100">
        <Container>
          <Row className="justify-content-center">
            <Col xl={6} md={10}>
              <div className="login-wrap">
                <div className="heading-s1 space-mb--20">
                  <h3>Đăng ký</h3>
                </div>
                <div>
                  <form method="post">
                    <div className="form-group">
                      <input
                        type="text"
                        required
                        className="form-control"
                        name="name"
                        placeholder="Nhập họ tên "
                        onChange={(e) => setName(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        required
                        className="form-control"
                        name="email"
                        placeholder="Nhập Email"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        required
                        className="form-control"
                        name="phone"
                        placeholder="Nhập số điện thoại"
                        onChange={(e) => setPhone(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        required
                        type="password"
                        name="password"
                        placeholder="Mật khẩu từ 6 đến 32 kí tự"
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        required
                        type="password"
                        name="cpassword"
                        placeholder="Nhập lại mật khẩu"
                        onChange={(e) => setConfirmPassword(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        placeholder="Ngày sinh"
                        type="text"
                        onFocus={(e) => {
                          e.currentTarget.type = "date";
                        }}
                        onBlur={(e) => {
                          e.currentTarget.type = "text";
                        }}
                        name="bod"
                        onChange={(e) => setDob(e.target.value)}
                      />
                    </div>
                    <Col className="form-group" md={12}>
                      <label>
                        Giới tính <span className="required">*</span>
                      </label>
                      <Form.Group className="mb-0">
                        <Form.Check
                          className="mx-3"
                          inline
                          type="radio"
                          checked
                          onChange={()=>setGender("Nam")}
                          label="Nam"
                          name="formHorizontalRadios"
                          id="formHorizontalRadios1"
                        />
                        <Form.Check
                          className="mx-3"
                          inline
                          type="radio"
                          onChange={()=>setGender("Nữ")}
                          label="Nữ"
                          name="formHorizontalRadios"
                          id="formHorizontalRadios2"
                        />
                      </Form.Group>
                    </Col>
                    <div className="form-group">
                      <button
                        type="submit"
                        className="btn btn-fill-out btn-block"
                        name="login"
                        onClick={(e) => {
                          e.preventDefault();
                          register();
                        }}
                      >
                        Đăng ký
                      </button>
                    </div>
                    <div className="login-footer form-group">
                      <div className="check-form">
                        <div className="custom-checkbox">
                          <label>
                            <span className="span-terms">
                              Khi bạn nhấn Đăng ký, bạn đã đồng ý thực hiện mọi
                              giao dịch mua bán theo{" "}
                              <a href="/legaldoc/terms">
                                điều kiện sử dụng và chính sách của Rent Mart.
                              </a>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div className="different-login">
                    <span> Hoặc</span>
                  </div>
                  <ul className="btn-login text-center">
                    <li>
                      <a href="#" className="btn btn-facebook">
                        <FaFacebookF />
                        Facebook
                      </a>
                    </li>
                    <li>
                      <a href="#" className="btn btn-google">
                        <FaGooglePlusG />
                        Google
                      </a>
                    </li>
                  </ul>
                  <div className="form-note text-center space-mt--20">
                    Bạn đã có tài khoản?{" "}
                    <Link href="/renter/login">
                      <a>Đăng nhập</a>
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </LayoutSix>
  );
};

export default normalRoute(Register);
