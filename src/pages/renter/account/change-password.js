import LayoutProfile from "../../../components/Account/LayoutProfile";
import { connect } from "react-redux";
import ChangePassword from "../../../components/Account/ChangePassword";
import { useToasts } from "react-toast-notifications";
import { updateToCustomer } from "../../../redux/actions/customerActions";
import { privateRoute } from "../../../components/withPrivateRoute";

const Password = ({ user, updateCustomer }) => {
  const { addToast } = useToasts();
  return (
    <LayoutProfile
      title="Thay đổi mật khẩu | Rent Mart "
      breadcrumbName="Thay đổi mật khẩu"
      activeKey="changePassword"
    >
      <ChangePassword user={user} addToast={addToast} />
    </LayoutProfile>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCustomer: (item, addToast) => {
      dispatch(updateToCustomer(item, addToast));
    },
  };
};

export default privateRoute(
  connect(mapStateToProps, mapDispatchToProps)(Password)
);
