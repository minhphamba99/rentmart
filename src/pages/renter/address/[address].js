import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import locationService from "../../../apis/location.api";
import customerService from "../../../apis/customer.api";
import LayoutProfile from "../../../components/Account/LayoutProfile";
import { Col } from "react-bootstrap";
import { useToasts } from "react-toast-notifications";
import { privateRoute } from "../../../components/withPrivateRoute";
import { connect } from "react-redux";

const FormAddress = ({ user }) => {
  const router = useRouter();
  const [phone, setPhone] = useState("");
  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);
  const [streetAndNumber, setStreetAndNumber] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [ward, setWard] = useState("");
  const [isDefault, setIsDefault] = useState(false);
  const { addToast } = useToasts();
  const id = router.query.aid;

  useEffect(() => {
    async function getCities() {
      await locationService
        .getCities()
        .then((data) => {
          setCities(data.sort((a, b) => a.cityName.localeCompare(b.cityName)));
        })
        .catch((err) => {
          setCities([]);
        });
    }
    getCities();
  }, []);

  const handleCity = async (cityCode) => {
    setCity(cities.filter((city) => city.cityCode == cityCode)[0]);
    setDistrict("");
    setWard("");
    setWards([]);
    setDistricts([]);
    await locationService
      .getDistricts(cityCode)
      .then((data) => {
        setDistricts(
          data.sort((a, b) => a.districtName.localeCompare(b.districtName))
        );
      })
      .catch((err) => {
        setWards([]);
        setDistricts([]);
      });
  };

  const handleDistrict = async (districtCode) => {
    setDistrict(
      districts.filter((district) => district.districtCode == districtCode)[0]
    );
    setWard("");
    setWards([]);
    await locationService
      .getWards(districtCode)
      .then((data) => {
        setWards(data.sort((a, b) => a.wardName.localeCompare(b.wardName)));
      })
      .catch((err) => {
        setWards([]);
      });
  };

  const handleWard = (wardCode) => {
    setWard(wards.filter((ward) => ward.wardCode == wardCode)[0]);
  };
  useEffect(() => {
    if (id) {
      async function getDelivery() {
        await customerService
          .getDeliverAddressById(user._id, id)
          .then(async (response) => {
            const add = response[0];
            setPhone(add.phoneNumber);
            setStreetAndNumber(add.streetAndNumber);
            setIsDefault(add.selected);

            await handleCity(add.cityCode).then(() => {
              setCity({
                cityCode: add.cityCode,
                cityName: add.cityName,
                cityPrefix: add.cityPrefix,
              });
            });
            await handleDistrict(add.districtCode).then(() => {
              setDistrict({
                districtCode: add.districtCode,
                districtName: add.districtName,
                districtPrefix: add.districtPrefix,
              });
              setWard({
                wardCode: add.wardCode,
                wardName: add.wardName,
                wardPrefix: add.wardPrefix,
              });
            });
          }).catch((err) => {
            router.push("/not-found");
          });
      }
      getDelivery();
    }
  }, []);

  const handleCreate = async () => {
    const input = {
      fullName: user.fullName,
      phoneNumber: phone,
      selected: isDefault,
      streetAndNumber: streetAndNumber,
      wardName: ward.wardName,
      wardCode: ward.wardCode,
      wardPrefix: ward.wardPrefix,
      districtName: district.districtName,
      districtCode: district.districtCode,
      districtPrefix: district.districtPrefix,
      cityName: city.cityName,
      cityCode: city.cityCode,
      cityPrefix: city.cityPrefix,
    };
    if (id) {
      await customerService
        .updateDeliverAddressById(id, input)
        .then(() => {
          addToast("Cập nhật địa chỉ thành công!", {
            appearance: "success",
            autoDismiss: true,
          });
          router.push("/renter/address");
        })
        .catch((error) => {
          addToast("Cập nhật địa chỉ thất bại!", {
            appearance: "error",
            autoDismiss: true,
          });
        });
    } else {
      await customerService
        .createDeliveryAddress(input)
        .then(() => {
          addToast("Tạo địa chỉ thành công!", {
            appearance: "success",
            autoDismiss: true,
          });
          router.push("/renter/address");
        })
        .catch((error) => {
          addToast("Tạo địa chỉ thất bại!", {
            appearance: "error",
            autoDismiss: true,
          });
        });
    }
  };
  return (
    <LayoutProfile
      title="Sổ địa chỉ (Address book) | Rent Mart "
      breadcrumbName="Tạo sổ địa chỉ"
    >
      <form method="post">
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            name="name"
            value={user.fullName}
            placeholder="Nhập họ tên "
            disabled
          />
        </div>
        <div className="form-group">
          <input
            type="text"
            required
            className="form-control"
            name="phone"
            placeholder="Nhập số điện thoại"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
        </div>

        <div className="form-group">
          <div className="custom-select-wrapper">
            <select
              name="city"
              className="first-null slt-city"
              value={city?.cityCode}
              onChange={(e) => handleCity(e.target.value)}
            >
              <option value={-1}>Chọn Tỉnh/Thành phố</option>
              {cities.map((city) => (
                <option key={city.cityCode} value={city.cityCode}>
                  {city.cityName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="form-group">
          <div className="custom-select-wrapper">
            <select
              name="district"
              className="first-null slt-city"
              value={district?.districtCode}
              onChange={(e) => handleDistrict(e.target.value)}
            >
              <option value={-2}>Chọn Quận/Huyện</option>
              {districts?.map((district) => (
                <option
                  key={district.districtCode}
                  value={district.districtCode}
                >
                  {district.districtPrefix} {district.districtName}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="form-group">
          <div className="custom-select-wrapper">
            <select
              name="ward"
              className="first-null slt-city"
              value={ward?.wardCode}
              onChange={(e) => handleWard(e.target.value)}
            >
              <option value={-3}>Chọn Phường/Xã</option>
              {wards?.map((ward) => (
                <option key={ward.wardCode} value={ward.wardCode}>
                  {ward.wardPrefix} {ward.wardName}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div className="form-group">
          <textarea
            rows="3"
            className="form-control h-100"
            name="streetAndNumber"
            placeholder="Nhập số địa chỉ"
            value={streetAndNumber}
            onChange={(e) => setStreetAndNumber(e.target.value)}
          />
        </div>
        <div className="form-group check-form">
          <div className="custom-checkbox">
            <input
              className="check-input"
              type="checkbox"
              name="checkbox"
              id="exampleCheckbox1"
              checked={isDefault}
              onChange={() => setIsDefault(!isDefault)}
            />
            <label className="check-label" htmlFor="exampleCheckbox1">
              <span>Đặt làm địa chỉ mặc định</span>
            </label>
          </div>
        </div>
        <Col className="text-center" md={12}>
          <button
            type="button"
            className="btn btn-fill-out"
            name="submit"
            value="Submit"
            onClick={handleCreate}
          >
            Cập nhật
          </button>
        </Col>
      </form>
    </LayoutProfile>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};

export default privateRoute(connect(mapStateToProps)(FormAddress));
