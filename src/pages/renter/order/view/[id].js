import LayoutProfile from "../../../../components/Account/LayoutProfile";
import orderService from "../../../../apis/order.api";
import { useEffect, useState } from "react";
import Link from "next/link";
import CardLoad from "../../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../../components/withPrivateRoute";
import {
  formatDate,
  formatPrice,
  getAddress,
  orderStatus,
  formatDateAndHour,
  paymentMethod,
} from "../../../../lib/utilities";
import { useRouter } from "next/router";
import ModalReview from "../../../../components/Account/ModalReview";

const DetailOrder = () => {
  const router = useRouter();
  const [order, setOrder] = useState();
  const [show, setShow] = useState(false);
  const handleCancel = async (id) => {
    await orderService
      .updateOrderById(id, "cancel")
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => {
      });
  };
  useEffect(() => {
    async function getMyOrder() {
      await orderService
        .getOrderById(router.query.id)
        .then((response) => {
          setOrder(response);
        })
        .catch(() => {
          router.push("/not-found");
        });
    }
    getMyOrder();
  }, []);
  return (
    <LayoutProfile
      title="Đơn hàng của tôi | Rent Mart "
      breadcrumbName="Đơn hàng của tôi"
      activeKey="order"
    >
      {order ? (
        <>
          <div className="edAZXd">
            <div className="bqOhOA">
              <div className="heading">
                <span>Chi tiết đơn hàng #{order._id}</span>
                <span className="split">-</span>
                <span className="status">{orderStatus(order.status)}</span>
              </div>
              <div className="created-date">
                Ngày đặt hàng:{" "}
                {formatDateAndHour(order.history.waitingToConfirm)}
              </div>
              {order.status === "returned" && (
                <div className="view-invoices">Xem hoá đơn</div>
              )}
              {/* <div className="jkHrCc">
                <div className="title">Thông báo</div>
                <div className="content">
                  <div className="notifications">
                    <div className="notifications__item">
                      <div className="date">08:35 12/04/2021</div>
                      <div className="comment">
                        Đơn hàng #{order._id} đã sẵn sàng để giao đến quý khách.
                        Chúng tôi vừa bàn giao đơn hàng của quý khách đến đối
                        tác vận chuyển của shop. Đơn hàng sẽ được giao trước
                        23:59 ngày {formatDate(order.product.beginTime)}
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
              <div className="kHWfJY">
                <div className="ipnhKS">
                  <div className="title">Địa chỉ người nhận</div>
                  <div className="content">
                    <p className="name">{order.fullName}</p>
                    <p className="address">
                      <span>Địa chỉ: </span>
                      {getAddress(order.deliveryAddress)}
                    </p>
                    <p className="phone">
                      <span>Điện thoại: </span>
                      {order.phoneNumber}
                    </p>
                  </div>
                </div>
                <div className="ipnhKS">
                  <div className="title">Hình thức giao hàng</div>
                  <div className="content">
                    <p>Giao hàng tiêu chuẩn</p>
                    <p>Phí vận chuyển: Free ship</p>
                  </div>
                </div>
                <div className="ipnhKS">
                  <div className="title">Hình thức thanh toán</div>
                  <div className="content">
                    <p className="">{paymentMethod(order.payment.method)}</p>
                  </div>
                </div>
              </div>
              <table className="cbhdrn">
                <thead>
                  <tr>
                    <th>Sản phẩm</th>
                    <th>Giá</th>
                    <th>Thời gian thuê</th>
                    <th>Giảm giá</th>
                    <th>Tạm tính</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div className="product-item">
                        <img
                          src={order.product.thumbnail}
                          alt={order.product.name}
                        />
                        <div className="product-info">
                          <Link
                            href={{
                              pathname: "/product/[slug]",
                              query: {
                                slug: `${order.product.name.replaceAll(
                                  " ",
                                  "-"
                                )}`,
                                spid: order.product.templateId,
                              },
                            }}
                          >
                            <a className="product-name">
                              {order.product.name} - {order.product.productCode}
                            </a>
                          </Link>
                          <p className="product-seller">
                            Cung cấp bởi &nbsp;
                            <Link
                              href={{
                                pathname: "/shop/[shop]",
                                query: {
                                  shop: `${order.agency.agencyName.replaceAll(
                                    " ",
                                    "-"
                                  )}`,
                                  sid: order.agencyId,
                                },
                              }}
                            >
                              <a> {order.agency.agencyName}</a>
                            </Link>
                          </p>
                          <p className="product-sku">Sku: 1029031127271</p>
                          {order.status === "returned" && (
                            <div className="product-review">
                              {order.reviewProduct || (
                                <span onClick={() => setShow(true)}>
                                  Viết nhận xét
                                </span>
                              )}
                              <Link
                                href={{
                                  pathname: "/product/[slug]",
                                  query: {
                                    slug: `${order.product.name.replaceAll(
                                      " ",
                                      "-"
                                    )}`,
                                    spid: order.product.templateId,
                                  },
                                }}
                              >
                                <a target="_blank">Thuê lại</a>
                              </Link>
                            </div>
                          )}
                        </div>
                      </div>
                    </td>
                    <td className="price">{formatPrice(order.totalPrice)} ₫</td>
                    <td className="quantity">
                      {formatDate(order.product.beginTime)} -{" "}
                      {formatDate(order.product.endTime)}
                    </td>
                    <td className="discount-amount">0 ₫</td>
                    <td className="raw-total">
                      {formatPrice(order.totalPrice)} ₫
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="4">
                      <span>Tạm tính</span>
                    </td>
                    <td>{formatPrice(order.totalPrice)} ₫</td>
                  </tr>
                  <tr>
                    <td colSpan="4">
                      <span>Phí vận chuyển</span>
                    </td>
                    <td>Free ship</td>
                  </tr>
                  <tr>
                    <td colSpan="4">
                      <span>Tổng cộng</span>
                    </td>
                    <td>
                      <span className="sum">
                        {formatPrice(order.totalPrice)} ₫
                      </span>
                    </td>
                  </tr>
                </tfoot>
              </table>
              <a className="view-list-order" href="/renter/order/history">
                &lt;&lt; Quay lại đơn hàng của tôi
              </a>
              {order.status !== "cancel" && (
                <a
                  className="view-tracking-detail"
                  href={`/renter/order/tracking/${order._id}`}
                >
                  Theo dõi đơn hàng
                </a>
              )}
              {order.status === "waitingToConfirm" && (
                <button
                  className="btn-cancel-order"
                  onClick={() => handleCancel(order._id)}
                >
                  Huỷ đơn hàng
                </button>
              )}
            </div>
          </div>
          {!show || (
            <ModalReview
              product={order.product}
              agency={order.agency}
              setShow={setShow}
              orderId={order._id}
            />
          )}
        </>
      ) : (
        <CardLoad />
      )}
    </LayoutProfile>
  );
};

export default privateRoute(DetailOrder);

// export async function getServerSideProps({ params }) {
//   try {
//     const order = await orderService.getOrderById(params.id);
//     return { props: { order } };
//   } catch {
//     // return {
//     //   redirect: {
//     //     destination: "/not-found",
//     //     permanent: false,
//     //   },
//     // };
//     return { props: {  } };
//   }
// }
