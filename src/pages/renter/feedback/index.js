import LayoutProfile from "../../../components/Account/LayoutProfile";
import orderService from "../../../apis/order.api";
import { useEffect, useState } from "react";
import Link from "next/link";
import CardLoad from "../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../components/withPrivateRoute";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";
import ModalReview from "../../../components/Account/ModalReview";

const FeedBackPage = ({ user }) => {
  const [loading, setLoading] = useState(true);
  // const router = useRouter();
  const [orders, setOrders] = useState([]);
  const [ordersFilter, setOrderFilter] = useState([]);
  const [show, setShow] = useState(false);

  useEffect(() => {
    async function getMyOrder() {
      await orderService
        .getMyOrder()
        .then((response) => {
          setOrders(response);
          setOrderFilter(response);
          setLoading(false);
        })
        .catch(() => {
          router.push("not-found");
        });
    }
    getMyOrder();
  }, []);

  return (
    <LayoutProfile
      title="Nhận xét sản phẩm đã thuê | Rent Mart "
      breadcrumbName="Nhận xét sản phẩm đã thuê"
      activeKey="feedback"
    >
      {orders.length > 0 && (
        <div className="my-reviews__inner">
          {orders.map((item, index) => {
            var agency = { agencyName: item.agencyName };
            if (item.status === "returned" && !item.reviewProduct)
              return (
                <div className="my-reviews__item">
                  <Link
                    key={index}
                    href={{
                      pathname: "/product/[slug]",
                      query: {
                        slug: `${item.product.name.replaceAll(" ", "-")}`,
                        spid: item.product.templateId,
                      },
                    }}
                  >
                    <a>
                      <div className="my-reviews__info">
                        <img
                          src={item.product.thumbnail}
                          alt={item.product.name}
                        />
                        <div className="my-reviews__name">
                          {item.product.name}
                        </div>
                      </div>
                    </a>
                  </Link>
                  <button
                    className="my-reviews__button"
                    onClick={() => setShow(true)}
                  >
                    Viết nhận xét
                  </button>
                  {!show || (
                    <ModalReview
                      product={item.product}
                      agency={agency}
                      setShow={setShow}
                      orderId={item._id}
                    />
                  )}
                </div>
              );
          })}
        </div>
      )}
      {!loading && orders.length === 0 && (
        <p>Bạn chưa có sản phẩm nào để đánh giá.</p>
      )}
      {loading && <CardLoad />}
    </LayoutProfile>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};
export default privateRoute(connect(mapStateToProps)(FeedBackPage));
