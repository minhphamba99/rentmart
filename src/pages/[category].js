import React, { useState, useEffect } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import Paginator from "react-hooks-paginator";
import { LayoutSix } from "../layouts";
import { BreadcrumbOne } from "../components/Breadcrumb";
import { Sidebar, OptionHeader, GridProducts } from "../components/Shop";
import { getSortedProducts } from "../lib/product";
import {
  fetchProductsCategory,
  fetchProducts,
} from "../redux/actions/productActions";
import { useRouter } from "next/router";
import { bindActionCreators } from "redux";
import productService from "../apis/product.api";

const ListProduct = ({ productList, setProducts }) => {
  const router = useRouter();
  const products = productList?.products;
  const { ctg, stg, q, category, sort, page } = router.query;
  const titleBreacdCurmb =
    category === "search" ? q : router.query.category.replaceAll("-", " ");
  const [layout, setLayout] = useState("grid");
  // const [sortType, setSortType] = useState("");
  // const [sortValue, setSortValue] = useState("");
  // const [filterSortType, setFilterSortType] = useState("");
  // const [filterSortValue, setFilterSortValue] = useState("");
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(parseInt(page) || 1);
  // const [currentData, setCurrentData] = useState(products);
  // const [sortedProducts, setSortedProducts] = useState([]);
  const [shopTopFilterStatus, setShopTopFilterStatus] = useState(false);
  const pageLimit = 60;
  useEffect(() => {
    setProducts(products);
  }, []);

  const getLayout = (layout) => {
    setLayout(layout);
  };

  const getSortParams = (sortType, sortValue) => {
    // setSortType(sortType);
    // setSortValue(sortValue);
  };
  function queryProduct({ sortFilter, pageFilter }) {
    router.push({
      pathname: "/[category]",
      query: {
        category: category,
        ctg: ctg,
        stg: stg,
        q: q,
        sort: sortFilter || sort,
        page: pageFilter || page,
      },
    });
  }
  const getFilterSortParams = (sortType, sortValue) => {
    let sortFilter =
      sortValue === "priceHighToLow" ? "rentPrice:-1" : "rentPrice:1";
    if (sortValue === "default") {
      sortFilter = "";
    }
    queryProduct({ sortFilter: sortFilter });
  };

  useEffect(() => {
    if (products) {
      queryProduct({ pageFilter: currentPage });
    }
  }, [currentPage]);

  return (
    <LayoutSix>
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Shop">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">{titleBreacdCurmb}</li>
        </ol>
      </BreadcrumbOne>
      <div className="shop-content space-pt--20 space-pb--r100">
        <Container>
          <Row>
            <Col lg={9}>
              {/* shop page header */}
              <OptionHeader
                getLayout={getLayout}
                getFilterSortParams={getFilterSortParams}
                shopTopFilterStatus={shopTopFilterStatus}
                setShopTopFilterStatus={setShopTopFilterStatus}
                layout={layout}
              />
              {/* shop products */}
              <GridProducts layout={layout} products={products} />
              {!products.length > 0 && (
                <div className="VLxTK">
                  <div>
                    Rất tiếc, không tìm thấy sản phẩm phù hợp với lựa chọn của
                    bạn
                  </div>
                </div>
              )}
              {/* shop product pagination */}
              <div className="pagination pagination-style pagination-style--two justify-content-center">
                <Paginator
                  totalRecords={productList.totalProducts}
                  pageLimit={pageLimit}
                  pageNeighbours={1}
                  setOffset={setOffset}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
                  pageContainerClass="mb-0 mt-0"
                  pagePrevText="«"
                  pageNextText="»"
                />
              </div>
            </Col>
            <Col lg={3} className="order-lg-first mt-4 pt-2 mt-lg-0 pt-lg-0">
              {/* sidebar */}
              <Sidebar products={products} getSortParams={getSortParams} />
            </Col>
          </Row>
        </Container>
      </div>
    </LayoutSix>
  );
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // getProductsCategory: fetchProductsCategory,
      setProducts: fetchProducts,
    },
    dispatch
  );
export default connect(null, mapDispatchToProps)(ListProduct);

export async function getServerSideProps({ query }) {
  try {
    const data = await productService.getProducts({
      categoriesId: query.ctg,
      subCategoriesId: query.stg,
      keyword: query.q,
      sort: query.sort,
      limit: 60,
      page: query.page,
    });
    // const products = data.products;
    // const totalProducts = data.totalProducts;
    return { props: { productList: data } };
  } catch {
    return {
      redirect: {
        destination: "/not-found",
        permanent: false,
      },
    };
  }
}
