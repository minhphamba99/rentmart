import axios from "axios";
// import Cookies from "universal-cookie";
import Cookie from "js-cookie";

const API_URL = "http:/localhost:4000/auths/";
// const axiosApiInstance = axios.create();
const axiosApiInstance = axios.create({
  baseURL: "http:/localhost:4000/auths/",
});
// const cookie = new Cookies();

// Request interceptor for API calls
axiosApiInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: "Bearer " + `${Cookie.get("accessToken")}`,
      "Content-Type": "application/json",
      Accept: "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if (error.response?.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      const token = await axiosApiInstance
        .post(
          `refresh-token`,
          { refreshToken: Cookie.get("refreshToken") }
          // {
          //   headers: {
          //     Authorization: "Bearer " + `${Cookie.get("accessToken")}`,
          //     "Content-Type": "application/json",
          //     Accept: "application/json",
          //   },
          // }
        )
        .then((response) => response)
        .catch((err) => {
          if (err.response?.status === 401) {
            Cookie.remove("accessToken");
            Cookie.remove("refreshToken");
          }
        });

      Cookie.set("accessToken", token?.data?.accessToken, { expires: 1 });
      Cookie.set("refreshToken", token?.data?.refreshToken, { expires: 1 });
      return axiosApiInstance(originalRequest);
    }
    return Promise.reject(error);
  }
);

export default axiosApiInstance;
