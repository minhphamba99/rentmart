import categoryService from '../../apis/category.api';

export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";


const fetchCategoriesSuccess = categories => ({
    type: FETCH_CATEGORIES_SUCCESS,
    payload: categories
});


// fetch categories
export const fetchCategories = () => {
    return dispatch => {
        categoryService.list().then((data) => {
            const categories = data;
            dispatch(fetchCategoriesSuccess(categories));
            return categories;
        }).catch((error) => {
        });
    };
};
