import productService from "../../apis/product.api";

export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_CATEGORY_PENDING =
  "FETCH_PRODUCTS_CATEGORY_PENDING";
export const FETCH_PRODUCTS_CATEGORY_ERROR = "FETCH_PRODUCTS_CATEGORY_ERROR";

export const FETCH_DETAIL_PRODUCT_SUCCESS = "FETCH__DETAIL_PRODUCT_SUCCESS";

const fetchProductsSuccess = (products) => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: products,
});

const fetchProductsCategoryPending = () => ({
  type: FETCH_PRODUCTS_CATEGORY_PENDING,
});

const fetchProductsCategoryError = (error) => ({
  type: FETCH_PRODUCTS_CATEGORY_ERROR,
  payload: error,
});

const fetchDetailProductsSuccess = (product) => ({
  type: FETCH_DETAIL_PRODUCT_SUCCESS,
  payload: product,
});

// fetch products
export const fetchProducts = (products) => {
  return (dispatch) => {
    dispatch(fetchProductsSuccess(products));
    return products;
  };
};

export const fetchDetailProducts = async (id) => {
  const product = await productService.getDetailProduct(id);
  return (dispatch) => {
    dispatch(fetchDetailProductsSuccess(product));
  };
};

export const fetchProductsCategory = (categoryId, keyword) => {
  return (dispatch) => {
    dispatch(fetchProductsCategoryPending());
    productService
      .getProducts({ categoryId, keyword })
      .then((data) => {
        const products = data.products;
        dispatch(fetchProductsSuccess(products));
        return products;
      })
      .catch((error) => {
        dispatch(fetchProductsCategoryError(error));
      });
  };
};
