import { v4 as uuidv4 } from "uuid";
import {
  ADD_TO_CART,
  DECREASE_QUANTITY,
  DELETE_FROM_CART,
  DELETE_ALL_FROM_CART,
  FETCH_CARTS_SUCCESS,
  LOGOUT_SUCCESS,
  UPDATE_FROM_CART,
} from "../actions/cartActions";

const initState = [];

const cartReducer = (state = initState, action) => {
  const cartItems = state,
    data = action.payload;
  if (action.type === LOGOUT_SUCCESS) {
    return [];
  }
  if (action.type === FETCH_CARTS_SUCCESS) {
    if (action.payload) return [...action.payload];
    return [];
  }

  if (action.type === ADD_TO_CART) {
    const cartItem = cartItems.filter(
      (item) => item.productId === data.productId
    )[0];

    if (cartItem === undefined) {
      return [...cartItems, data];
    } else if (
      cartItem !== undefined &&
      (cartItem.endTime !== data.endTime ||
        cartItem.beginTime !== data.beginTime)
    ) {
      return [...cartItems, data];
    } else {
      return cartItems.map((item) =>
        item.productId === cartItem.productId
          ? {
              ...item,
              quantity: data.quantity
                ? item.quantity + data.quantity
                : item.quantity + 1,
            }
          : item
      );
    }
  }

  if (action.type === DECREASE_QUANTITY) {
    if (data.quantity === 1) {
      const remainingItems = (cartItems, data) =>
        cartItems.filter((cartItem) => cartItem._id !== data._id);
      return remainingItems(cartItems, data);
    } else {
      return cartItems.map((item) =>
        item._id === data._id ? { ...item, quantity: item.quantity - 1 } : item
      );
    }
  }

  if (action.type === DELETE_FROM_CART) {
    const remainingItems = (cartItems, data) =>
      cartItems.filter((cartItem) => cartItem.productId !== data._id);
    return remainingItems(cartItems, data);
  }

  if (action.type === UPDATE_FROM_CART) {
    let carts = cartItems;
    carts.forEach(function (item) {
      if (item._id === data.item._id) {
        item.beginTime = data.time[0];
        item.endTime = data.time[1];
      }
    });
    return [...carts];
  }

  if (action.type === DELETE_ALL_FROM_CART) {
    return cartItems.filter((item) => {
      return false;
    });
  }

  return state;
};

export default cartReducer;
