import { FETCH_PRODUCTS_SUCCESS, FETCH_DETAIL_PRODUCT_SUCCESS , FETCH_PRODUCTS_CATEGORY_PENDING, FETCH_PRODUCTS_CATEGORY_ERROR } from "../actions/productActions";

const initState = [];

const productReducer = (state = initState, action) => {
  if (action.type === FETCH_PRODUCTS_SUCCESS) {
    return {
      ...state,
      pending: false,
      products: action.payload,
      error: false,
    };
  }
  if (action.type === FETCH_PRODUCTS_CATEGORY_PENDING) {
    return {
      ...state,
      pending: true,
      error: false,
      // products: [],
    };
  }
  if (action.type === FETCH_PRODUCTS_CATEGORY_ERROR) {
    return {
      ...state,
      pending: false,
      error: action.payload,
    };
  }


  if (action.type === FETCH_DETAIL_PRODUCT_SUCCESS) {
    return {
      ...state,
      product: action.payload.product,
    };
  }

  return state;
};

export default productReducer;
